/*
	Linear Congruential Generator

	Xn+1 = (aXn + c) mod m

	X 							= Sequence of pseudo-random values
	m, 0 < m 				= modulus
	a, 0 < a < m 		= multiplier
	c, 0 <= c < m 	= increment
	X0, 0 <= X0 < m = seed
*/

const not = boolExpr => !boolExpr;

function lcg(prev, m, a, c) {
	if (not(0 < m)) return new Error('Modulus must be greater than 0');

	if (not(0 < a)) return new Error('Multiplier must be greater than 0');
	if (not(a < m)) return new Error('Multiplier must be lesser than modulus');

	if (not(0 <= c)) return new Error('Increment must be greater than, or equal to, 0');
	if (not(c < m)) return new Error('Increment must be lesser than modulus');

	if (not(0 <= prev)) return new Error('Previous value must be greater than, or equal to, 0');
	if (not(prev < m)) return new Error('Previous value must be lesser than modulus');

	return (a * prev + c) % m;
}

function* generate(seed, m, a, c) {
	let prev = seed;
	let done = false;

	while (not(done)) {
		next = lcg(prev, m, a, c);

		if (next instanceof Error) {
			return next;
		}
		else {
			yield next;
		}

		prev = next;
	}
}

module.exports = generate;
